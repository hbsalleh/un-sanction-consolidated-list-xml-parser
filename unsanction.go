package main;

import (
	"fmt"
	"io/ioutil"
	"os"
	"encoding/xml"
	"strings"
	"bufio"
	"strconv"
)

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func processIndividualAlias(alias IndividualAlias) string {
	returnStr := "";

	if(len(alias.Quality) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Alias Quality: " + alias.Quality;
	}

	if(len(alias.AliasName) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Alias Name: " + alias.AliasName;
	}

	if(len(alias.BirthDate) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Date of Birth: " + alias.BirthDate;
	}

	if(len(alias.BirthCity) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "City of Birth: " + alias.BirthCity;
	}

	if(len(alias.BirthCountry) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Country of Birth: " + alias.BirthCountry;
	}

	if(len(alias.Note) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Alias Note: " + alias.Note;
	}

	return "{" + returnStr + "}";
}

func processOrganisationAlias(alias OrganisationAlias) string {
	returnStr := "";

	if(len(alias.Quality) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Alias Quality: " + alias.Quality;
	}

	if(len(alias.AliasName) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Alias Name: " + alias.AliasName;
	}

	return "{" + returnStr + "}";
}

func processIndividualAddress(address IndividualAddress) string {
	returnStr := "";

	if(len(address.Street) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Street: " + address.Street;
	}

	if(len(address.City) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "City: " + address.City;
	}

	if(len(address.Province) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Province: " + address.Province;
	}

	if(len(address.PostalCode) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Postal Code: " + address.PostalCode;
	}

	if(len(address.Country) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Country: " + address.Country;
	}

	if(len(address.Note) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Note: " + address.Note;
	}

	return "{" + returnStr + "}";
}

func processOrganisationAddress(address OrganisationAddress) string {
	returnStr := "";

	if(len(address.Street) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Street: " + address.Street;
	}

	if(len(address.City) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "City: " + address.City;
	}

	if(len(address.Province) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Province: " + address.Province;
	}

	if(len(address.PostalCode) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Postal Code: " + address.PostalCode;
	}

	if(len(address.Country) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Country: " + address.Country;
	}

	if(len(address.Note) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Note: " + address.Note;
	}

	return "{" + returnStr + "}";
}

func processIndividualDOB(dob IndividualDOB) string {
	returnStr := "";

	if(len(dob.DateType) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "DateType: " + dob.DateType;
	}

	if(len(dob.Note) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Note: " + dob.Note;
	}

	if(len(dob.Date) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Date: " + dob.Date;
	}

	if(len(dob.Year) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Year: " + dob.Year;
	}

	if(len(dob.FromYear) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "From: " + dob.FromYear;
	}

	if(len(dob.ToYear) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "To: " + dob.ToYear;
	}

	return "{" + returnStr + "}";
}

func processIndividualPOB(pob IndividualPOB) string {
	returnStr := "";

	if(len(pob.City) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "City: " + pob.City;
	}

	if(len(pob.Province) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Province: " + pob.Province;
	}

	if(len(pob.Country) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Country: " + pob.Country;
	}

	if(len(pob.Note) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Note: " + pob.Note;
	}

	return "{" + returnStr + "}";
}

func processIndividualDocument(doc IndividualDocument) string {
	returnStr := "";

	if(len(doc.DocType1) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "DocumentType: " + strings.TrimSpace(doc.DocType1);
	}

	if(len(doc.DocType2) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "DocumentType2: " +  strings.TrimSpace(doc.DocType2);
	}

	if(len(doc.DocNo) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "DocumentNumber: " +  strings.TrimSpace(doc.DocNo);
	}

	if(len(doc.IssuingCountry) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "IssuingCountry: " +  strings.TrimSpace(doc.IssuingCountry);
	}

	if(len(doc.IssueDate) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "DateOfIssue: " +  strings.TrimSpace(doc.IssueDate);
	}

	if(len(doc.IssueCity) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "CityOfIssue: " +  strings.TrimSpace(doc.IssueCity);
	}

	if(len(doc.IssueCountry) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "CountryOfIssue: " +  strings.TrimSpace(doc.IssueCountry);
	}

	if(len(doc.Note) > 0) {
		if(len(returnStr) > 0) {
			returnStr += ", ";
		}
		returnStr += "Note: " +  strings.TrimSpace(doc.Note);
	}

	nlreplacer := strings.NewReplacer("\n", "");

	return "{" + nlreplacer.Replace(strings.TrimSpace(returnStr)) + "}";
}

func main() {
	// Open our xmlFile
    xmlFile, err := os.Open("UNSanction-Consolidated.xml");
    // if we os.Open returns an error then handle it
    if err != nil {
        fmt.Println(err);
    } else {
		fmt.Println("Successfully Opened file ", xmlFile.Name());
		// defer the closing of our xmlFile so that we can parse it later on
		defer xmlFile.Close();

		// read our opened xmlFile as a byte array.
		byteValue, _ := ioutil.ReadAll(xmlFile);

		var consList ConsolidatedList;

		xml.Unmarshal(byteValue, &consList);

		fileHandle, err := os.Create("tblS_UNSanction_V2.sql");
		check(err);
		defer fileHandle.Close();
		bufWriter := bufio.NewWriter(fileHandle);
		
		replacer := strings.NewReplacer("'", "''");
		nlreplacer := strings.NewReplacer("\n", "");

		headerStr := "INSERT INTO [tblS_UNSanction_V2] ([UNS_ID],[ListType],[EntityType],[EntityName],[ConsolidatedInfo]) VALUES";
		nB, err := bufWriter.WriteString(headerStr + "\n");
		check(err);
		fmt.Printf("wrote %d bytes of header\n\n", nB);
		bufWriter.Flush();

		fmt.Println(">>>Individual List:");
		for i := 0; i < len(consList.Individuals.Individual); i++ {
			
			
			fmt.Println("UN DataID:", consList.Individuals.Individual[i].DataID);
			fmt.Println("List Type: " + nlreplacer.Replace(strings.TrimSpace(consList.Individuals.Individual[i].UNListType)));
			indName := consList.Individuals.Individual[i].FirstName + ", " +
				consList.Individuals.Individual[i].SecondName +
				" " + consList.Individuals.Individual[i].ThirdName +
				" " + consList.Individuals.Individual[i].FourthName;
			fmt.Println("Name: " + nlreplacer.Replace(strings.TrimSpace(indName)));

			consInfo := "";

			// Process Version Number
			versionNumStr := "Version: [" + strconv.Itoa(consList.Individuals.Individual[i].VersionNo) + "]";

			if(len(versionNumStr) > 0) {
				fmt.Println(versionNumStr);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += versionNumStr;
			}

			// Process Reference Number
			refNumStr := ""
			if(len(consList.Individuals.Individual[i].ReferenceNo) > 0) {
				refNumStr = "Reference: [" + consList.Individuals.Individual[i].ReferenceNo + "]";
			}

			if(len(refNumStr) > 0) {
				fmt.Println(refNumStr);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += refNumStr;
			}

			// Process Submitted By
			submittedBy := ""
			if(len(consList.Individuals.Individual[i].SubmittedBy) > 0) {
				submittedBy = "SubmittedBy: [" + consList.Individuals.Individual[i].SubmittedBy + "]";
			}

			if(len(submittedBy) > 0) {
				fmt.Println(submittedBy);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += submittedBy;
			}

			// Process Gender
			gender := ""
			if(len(consList.Individuals.Individual[i].Gender) > 0) {
				gender = "Gender: [" + consList.Individuals.Individual[i].Gender + "]";
			}

			if(len(gender) > 0) {
				fmt.Println(gender);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += gender;
			}

			// Process Original Name Script
			orinameScript := ""
			if(len(consList.Individuals.Individual[i].OriginalNameScript) > 0) {
				orinameScript = "OriginalNameScript: [" + consList.Individuals.Individual[i].OriginalNameScript + "]";
			}

			if(len(orinameScript) > 0) {
				fmt.Println(orinameScript);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += orinameScript;
			}

			// Process Titles
			titleList := "";
			if(len(consList.Individuals.Individual[i].Titles.Title) > 0) {
				titleList = "Title: [";

				for j := 0; j < len(consList.Individuals.Individual[i].Titles.Title); j++ {
					if(j > 0){
						titleList += ",";
					}
					titleList += consList.Individuals.Individual[i].Titles.Title[j];
				}

				titleList += "]";
			}

			if(len(titleList) > 0) {
				fmt.Println(titleList);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += titleList;
			}

			// Process Designations
			designationList := "";
			if(len(consList.Individuals.Individual[i].Designations.Designation) > 0) {
				designationList = "Designation: [";

				for j := 0; j < len(consList.Individuals.Individual[i].Designations.Designation); j++ {
					if(j > 0){
						designationList += ",";
					}
					designationList += consList.Individuals.Individual[i].Designations.Designation[j];
				}

				designationList += "]";
			}

			if(len(designationList) > 0) {
				fmt.Println(designationList);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += designationList;
			}

			// Process Nationalities
			nationalityList := "";
			if(len(consList.Individuals.Individual[i].Nationalities.Nationality) > 0) {
				nationalityList = "Nationality: [";

				for j := 0; j < len(consList.Individuals.Individual[i].Nationalities.Nationality); j++ {
					if(j > 0){
						nationalityList += ",";
					}
					nationalityList += consList.Individuals.Individual[i].Nationalities.Nationality[j];
				}

				nationalityList += "]";
			}

			if(len(nationalityList) > 0) {
				fmt.Println(nationalityList);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += nationalityList;
			}

			// Process Nationality2
			nationality2 := ""
			if(len(consList.Individuals.Individual[i].Nationality2) > 0) {
				nationality2 = "Nationality2: [" + consList.Individuals.Individual[i].Nationality2 + "]";
			}

			if(len(nationality2) > 0) {
				fmt.Println(nationality2);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += nationality2;
			}

			// Process Alias
			aliasList := "";
			if(len(consList.Individuals.Individual[i].Alias) > 0) {
				aliasList = "Alias: [";

				for j := 0; j < len(consList.Individuals.Individual[i].Alias); j++ {
					if(j > 0) {
						aliasList += ",";
					}
					aliasList += processIndividualAlias(consList.Individuals.Individual[i].Alias[j]);
				}

				aliasList += "]";
			}

			if(len(aliasList) > 0) {
				fmt.Println(aliasList);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += aliasList;
			}

			// Process Update Dates
			updateList := "";
			if(len(consList.Individuals.Individual[i].UpdateDates.UpdateDate) > 0) {
				updateList = "UpdateDate: [";

				for j := 0; j < len(consList.Individuals.Individual[i].UpdateDates.UpdateDate); j++ {
					if(j > 0){
						updateList += ",";
					}
					updateList += consList.Individuals.Individual[i].UpdateDates.UpdateDate[j];
				}

				updateList += "]";
			}

			if(len(updateList) > 0) {
				fmt.Println(updateList);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += updateList;
			}

			// Process Address
			addressList := "";
			if(len(consList.Individuals.Individual[i].Address) > 0) {
				addressList = "Address: [";

				for j := 0; j < len(consList.Individuals.Individual[i].Address); j++ {
					if(j > 0) {
						addressList += ",";
					}
					addressList += processIndividualAddress(consList.Individuals.Individual[i].Address[j]);
				}

				addressList += "]";
			}

			if(len(addressList) > 0) {
				fmt.Println(addressList);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += addressList;
			}

			// Process DOB
			dobList := "";
			if(len(consList.Individuals.Individual[i].DOB) > 0) {
				dobList = "DateOfBirth: [";

				for j := 0; j < len(consList.Individuals.Individual[i].DOB); j++ {
					if(j > 0) {
						dobList += ",";
					}
					dobList += processIndividualDOB(consList.Individuals.Individual[i].DOB[j]);
				}

				dobList += "]";
			}

			if(len(dobList) > 0) {
				fmt.Println(dobList);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += dobList;
			}

			// Process POB
			pobList := "";
			if(len(consList.Individuals.Individual[i].POB) > 0) {
				pobList = "PlaceOfBirth: [";

				for j := 0; j < len(consList.Individuals.Individual[i].POB); j++ {
					if(j > 0) {
						pobList += ",";
					}
					pobList += processIndividualPOB(consList.Individuals.Individual[i].POB[j]);
				}

				pobList += "]";
			}

			if(len(pobList) > 0) {
				fmt.Println(pobList);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += pobList;
			}

			// Process Document
			docList := "";
			if(len(consList.Individuals.Individual[i].Document) > 0) {
				docList = "Document: [";

				for j := 0; j < len(consList.Individuals.Individual[i].Document); j++ {
					if(j > 0) {
						docList += ",";
					}
					docList += processIndividualDocument(consList.Individuals.Individual[i].Document[j]);
				}

				docList += "]";
			}

			if(len(docList) > 0) {
				fmt.Println(docList);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += docList;
			}

			// Process Listed On
			listedOn := "";
			if(len(consList.Individuals.Individual[i].ListedOn) > 0) {
				listedOn = "ListedOn: [" + consList.Individuals.Individual[i].ListedOn + "]";
			}

			if(len(listedOn) > 0) {
				fmt.Println(listedOn);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += listedOn;
			}

			// Process Delisted On
			delistedOn := "";
			if(len(consList.Individuals.Individual[i].DelistedOn) > 0) {
				delistedOn = "DelistedOn: [" + consList.Individuals.Individual[i].DelistedOn + "]";
			}

			if(len(delistedOn) > 0) {
				fmt.Println(delistedOn);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += delistedOn;
			}
			
			// Process Comment
			fmt.Println("Comment: " + consList.Individuals.Individual[i].Comment);
			if(len(consList.Individuals.Individual[i].Comment) > 0) {
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += "Comment: [" + consList.Individuals.Individual[i].Comment + "]";
			}
			
			lineStr := fmt.Sprintf("(%d,'%s','individual','%s','%s'),",
							consList.Individuals.Individual[i].DataID,
							replacer.Replace(nlreplacer.Replace(strings.TrimSpace(consList.Individuals.Individual[i].UNListType))),
							replacer.Replace(nlreplacer.Replace(strings.TrimSpace(indName))),
							replacer.Replace(nlreplacer.Replace(strings.TrimSpace(consInfo))));

			writtenBytes, err := bufWriter.WriteString(lineStr + "\n")
			check(err)
			fmt.Printf("wrote %d bytes to file %s\n\n", writtenBytes, fileHandle.Name())
			bufWriter.Flush()
		}

		
		fmt.Println("\n>>>Organisation List:")
		for i := 0; i < len(consList.Organisations.Organisation); i++ {
			fmt.Println("UN DataID:", consList.Organisations.Organisation[i].DataID);
			fmt.Println("List Type: " + consList.Organisations.Organisation[i].UNListType);
			fmt.Println("Name: " + strings.TrimSpace(consList.Organisations.Organisation[i].OrganisationName));


			consInfo := "";

			// Process Version Number
			versionNumStr := "Version: [" + strconv.Itoa(consList.Organisations.Organisation[i].VersionNo) + "]";

			if(len(versionNumStr) > 0) {
				fmt.Println(versionNumStr);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += versionNumStr;
			}

			// Process Reference Number
			refNumStr := ""
			if(len(consList.Organisations.Organisation[i].ReferenceNo) > 0) {
				refNumStr = "Reference: [" + consList.Organisations.Organisation[i].ReferenceNo + "]";
			}

			if(len(refNumStr) > 0) {
				fmt.Println(refNumStr);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += refNumStr;
			}

			// Process Listed On
			listedOn := "";
			if(len(consList.Organisations.Organisation[i].ListedOn) > 0) {
				listedOn = "ListedOn: [" + consList.Organisations.Organisation[i].ListedOn + "]";
			}

			if(len(listedOn) > 0) {
				fmt.Println(listedOn);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += listedOn;
			}

			// Process Delisted On
			delistedOn := "";
			if(len(consList.Organisations.Organisation[i].DelistedOn) > 0) {
				delistedOn = "DelistedOn: [" + consList.Organisations.Organisation[i].DelistedOn + "]";
			}

			if(len(delistedOn) > 0) {
				fmt.Println(delistedOn);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += delistedOn;
			}

			// Process Submitted On
			submittedOn := ""
			if(len(consList.Organisations.Organisation[i].SubmittedOn) > 0) {
				submittedOn = "SubmittedOn: [" + consList.Organisations.Organisation[i].SubmittedOn + "]";
			}

			if(len(submittedOn) > 0) {
				fmt.Println(submittedOn);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += submittedOn;
			}

			// Process Original Name Script
			orinameScript := ""
			if(len(consList.Organisations.Organisation[i].OriginalNameScript) > 0) {
				orinameScript = "OriginalNameScript: [" + consList.Organisations.Organisation[i].OriginalNameScript + "]";
			}

			if(len(orinameScript) > 0) {
				fmt.Println(orinameScript);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += orinameScript;
			}

			// Process Update Dates
			updateList := "";
			if(len(consList.Organisations.Organisation[i].UpdateDates.UpdateDate) > 0) {
				updateList = "UpdateDate: [";

				for j := 0; j < len(consList.Organisations.Organisation[i].UpdateDates.UpdateDate); j++ {
					if(j > 0){
						updateList += ",";
					}
					updateList += consList.Organisations.Organisation[i].UpdateDates.UpdateDate[j];
				}

				updateList += "]";
			}

			if(len(updateList) > 0) {
				fmt.Println(updateList);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += updateList;
			}

			// Process Alias
			aliasList := "";
			if(len(consList.Organisations.Organisation[i].Alias) > 0) {
				aliasList = "Alias: [";

				for j := 0; j < len(consList.Organisations.Organisation[i].Alias); j++ {
					if(j > 0) {
						aliasList += ",";
					}
					aliasList += processOrganisationAlias(consList.Organisations.Organisation[i].Alias[j]);
				}

				aliasList += "]";
			}

			// Process Address
			addressList := "";
			if(len(consList.Organisations.Organisation[i].Address) > 0) {
				addressList = "Address: [";

				for j := 0; j < len(consList.Organisations.Organisation[i].Address); j++ {
					if(j > 0) {
						addressList += ",";
					}
					addressList += processOrganisationAddress(consList.Organisations.Organisation[i].Address[j]);
				}

				addressList += "]";
			}

			if(len(addressList) > 0) {
				fmt.Println(addressList);
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += addressList;
			}

			// Process Comment
			fmt.Println("Comment: " + consList.Organisations.Organisation[i].Comment);
			if(len(consList.Organisations.Organisation[i].Comment) > 0) {
				if(len(consInfo) > 0){
					consInfo += " ";
				}
				consInfo += "Comment: [" + consList.Organisations.Organisation[i].Comment + "]";
			}

			lineStr := fmt.Sprintf("(%d,'%s','organisation','%s','%s'),",
							consList.Organisations.Organisation[i].DataID,
							replacer.Replace(nlreplacer.Replace(strings.TrimSpace(consList.Organisations.Organisation[i].UNListType))),
							replacer.Replace(nlreplacer.Replace(strings.TrimSpace(consList.Organisations.Organisation[i].OrganisationName))),
							replacer.Replace(nlreplacer.Replace(strings.TrimSpace(consInfo))));

			writtenBytes, err := bufWriter.WriteString(lineStr + "\n");
			check(err);
			fmt.Printf("wrote %d bytes\n\n", writtenBytes);
			bufWriter.Flush();
		}
		

		fmt.Println("\n>>>Total Individual Records:", len(consList.Individuals.Individual))
		fmt.Println(">>>Total Individual Records:", len(consList.Organisations.Organisation))
	}
}

// our struct which contains the complete
// array of all Users in the file
type ConsolidatedList struct {
    XMLName				xml.Name		`xml:"CONSOLIDATED_LIST"`
	Individuals			Individuals		`xml:"INDIVIDUALS"`
	Organisations		Organisations	`xml:"ENTITIES"`
}

type Individuals struct {
	XMLName			xml.Name		`xml:"INDIVIDUALS"`
	Individual		[]Individual	`xml:"INDIVIDUAL"`
}

type Organisations struct {
	XMLName			xml.Name		`xml:"ENTITIES"`
	Organisation	[]Organisation	`xml:"ENTITY"`
}

type Individual struct {
	XMLName			xml.Name		`xml:"INDIVIDUAL"`
	DataID			int				`xml:"DATAID"`
	FirstName		string			`xml:"FIRST_NAME"`
	SecondName		string			`xml:"SECOND_NAME"`
	ThirdName		string			`xml:"THIRD_NAME"`
	FourthName		string			`xml:"FOURTH_NAME"`
	UNListType		string			`xml:"UN_LIST_TYPE"`
	VersionNo		int				`xml:"VERSIONNUM"`
	ReferenceNo		string			`xml:"REFERENCE_NUMBER"`
	ListedOn		string			`xml:"LISTED_ON"`
	DelistedOn		string			`xml:"DELISTED_ON"`
	Gender			string			`xml:"GENDER"`
	SubmittedBy		string			`xml:"SUBMITTED_BY"`
	OriginalNameScript	string		`xml:"NAME_ORIGINAL_SCRIPT"`
	Comment			string			`xml:"COMMENTS1"`
	Nationalities	Nationalities	`xml:"NATIONALITY"`
	Nationality2	string			`xml:"NATIONALITY2"`
	Titles			Titles			`xml:"TITLE"`
	Designations	Designations	`xml:"DESIGNATION"`
	UpdateDates		UpdateDates		`xml:"LAST_DAY_UPDATED"`
	Alias			[]IndividualAlias	`xml:"INDIVIDUAL_ALIAS"`
	Address			[]IndividualAddress	`xml:"INDIVIDUAL_ADDRESS"`
	DOB				[]IndividualDOB		`xml:"INDIVIDUAL_DATE_OF_BIRTH"`
	POB				[]IndividualPOB		`xml:"INDIVIDUAL_PLACE_OF_BIRTH"`
	Document		[]IndividualDocument	`xml:"INDIVIDUAL_DOCUMENT"`
}

type IndividualDocument struct {
	XMLName			xml.Name		`xml:"INDIVIDUAL_DOCUMENT"`
	DocType1		string			`xml:"TYPE_OF_DOCUMENT"`
	DocType2		string			`xml:"TYPE_OF_DOCUMENT2"`
	DocNo			string			`xml:"NUMBER"`
	IssuingCountry	string			`xml:"ISSUING_COUNTRY"`
	IssueDate		string			`xml:"DATE_OF_ISSUE"`
	IssueCity		string			`xml:"CITY_OF_ISSUE"`
	IssueCountry	string			`xml:"COUNTRY_OF_ISSUE"`
	Note			string			`xml:"NOTE"`
}

type IndividualAlias struct {
	XMLName			xml.Name		`xml:"INDIVIDUAL_ALIAS"`
	Quality			string			`xml:"QUALITY"`
	AliasName		string			`xml:"ALIAS_NAME"`
	BirthDate		string			`xml:"DATE_OF_BIRTH"`
	BirthCity		string			`xml:"CITY_OF_BIRTH"`
	BirthCountry	string			`xml:"COUNTRY_OF_BIRTH`
	Note			string			`xml:"NOTE"`		
}

type IndividualAddress struct {
	XMLName			xml.Name		`xml:"INDIVIDUAL_ADDRESS"`
	Street			string			`xml:"STREET"`
	City			string			`xml:"CITY"`
	Province		string			`xml:"STATE_PROVINCE"`
	PostalCode		string			`xml:"ZIP_CODE"`
	Country			string			`xml:"COUNTRY"`
	Note			string			`xml:"NOTE"`
}

type IndividualPOB struct {
	XMLName			xml.Name		`xml:"INDIVIDUAL_PLACE_OF_BIRTH"`
	City			string			`xml:"CITY"`
	Province		string			`xml:"STATE_PROVINCE"`
	Country			string			`xml:"COUNTRY"`
	Note			string			`xml:"NOTE"`
}

type IndividualDOB struct {
	XMLName			xml.Name		`xml:"INDIVIDUAL_DATE_OF_BIRTH"`
	DateType		string			`xml:"TYPE_OF_DATE"`
	Note			string			`xml:"NOTE"`
	Date			string			`xml:"DATE"`
	Year			string			`xml:"YEAR"`
	FromYear		string			`xml:"FROM_YEAR"`
	ToYear			string			`xml:"TO_YEAR`
}

type Nationalities struct {
	XMLName			xml.Name		`xml:"NATIONALITY"`
	Nationality		[]string		`xml:"VALUE"`
}

type Titles struct {
	XMLName			xml.Name		`xml:"TITLE"`
	Title			[]string		`xml:"VALUE"`
}

type Designations struct {
	XMLName			xml.Name		`xml:"DESIGNATION"`
	Designation		[]string		`xml:"VALUE"`
}

type UpdateDates struct {
	XMLName			xml.Name		`xml:"LAST_DAY_UPDATED"`
	UpdateDate		[]string		`xml:"VALUE"`
}

type Organisation struct {
	XMLName				xml.Name		`xml:"ENTITY"`
	DataID				int				`xml:"DATAID"`
	VersionNo			int				`xml:"VERSIONNUM"`
	OrganisationName	string			`xml:"FIRST_NAME"`
	UNListType			string			`xml:"UN_LIST_TYPE"`
	ReferenceNo			string			`xml:"REFERENCE_NUMBER"`
	ListedOn			string			`xml:"LISTED_ON"`
	DelistedOn			string			`xml:"DELISTED_ON"`
	SubmittedOn			string			`xml:"SUBMITTED_ON"`
	OriginalNameScript	string			`xml:"NAME_ORIGINAL_SCRIPT"`
	UpdateDates			UpdateDates		`xml:"LAST_DAY_UPDATED"`
	Comment				string			`xml:"COMMENTS1"`
	Alias		[]OrganisationAlias		`xml:"ENTITY_ALIAS"`
	Address		[]OrganisationAddress	`xml:"ENTITY_ADDRESS"`
}

type OrganisationAlias struct {
	XMLName			xml.Name		`xml:"ENTITY_ALIAS"`
	Quality			string			`xml:"QUALITY"`
	AliasName		string			`xml:"ALIAS_NAME"`		
}

type OrganisationAddress struct {
	XMLName			xml.Name		`xml:"ENTITY_ADDRESS"`
	Street			string			`xml:"STREET"`
	City			string			`xml:"CITY"`
	Province		string			`xml:"STATE_PROVINCE"`
	PostalCode		string			`xml:"ZIP_CODE"`
	Country			string			`xml:"COUNTRY"`
	Note			string			`xml:"NOTE"`
}